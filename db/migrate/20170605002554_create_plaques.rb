class CreatePlaques < ActiveRecord::Migration
  def change
    create_table :plaques do |t|
      t.string :number

      t.timestamps null: false
    end
  end
end

class Plaque < ActiveRecord::Base

  def initialize
  end

  class << self
    def can_circulate?(full_number, full_date)
      # Get times parameters
      get_times(full_date)
      # Number day of weekend
      day = DateTime.parse(full_date).wday
      # Last number for cars
      last_number = full_number.last(1)
      # Last number for motorcycles
      unless /^[0-9]+$/.match(last_number)
        last_number = full_number.last(2).first
      end
      # Last number to interger
      last_number = last_number.to_i
      # Can circulate on the road
      if @full_date.between?(@morning_from, @morning_to) || @full_date.between?(@afternoon_from, @afternoon_to)
        # weekdays
        if day == 1 && (last_number == 1 || last_number == 2)
          can_circulate = false
        elsif day == 2 && (last_number == 3 || last_number == 4)
          can_circulate = false
        elsif day == 3 && (last_number == 5 || last_number == 6)
          can_circulate = false
        elsif day == 4 && (last_number == 7 || last_number == 8)
          can_circulate = false
        elsif day == 5 && (last_number == 9 || last_number == 0)
          can_circulate = false
        # weekend
        elsif day == 6 || day == 0
          can_circulate = true
        end
      else
        can_circulate = true
      end
      return can_circulate
    end     


    def valid_plaque?(plaque)
      /[a-z]{3}[0-9]{4}/.match(plaque.downcase) || /[a-z]{2}[0-9]{3}[a-z]{1}/.match(plaque.downcase) ? true : false   
    end

    def valid_date?(date)
      DateTime.parse date rescue false
    end

    def get_times(full_date)
      @full_date = Time.parse(full_date)
      @morning_from = Time.parse("07:00:00")
      @morning_to = Time.parse("09:30:00")
      @afternoon_from = Time.parse("16:00:00")
      @afternoon_to = Time.parse("19:30:00")
    end


  end

end

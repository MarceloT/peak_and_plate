class PlaquesController < ApplicationController
  # before_action :set_plaque, only: [:show, :edit, :update, :destroy]

  # GET /plaques
  # GET /plaques.json
  def index
    @date = params[:date] || Time.now.to_s(:db)
  end

  def can_circulate
    full_number = params[:number]
    full_date = params[:date]

    Rails.logger.info ""
    pa = Plaque.new
    if Plaque.valid_plaque?(full_number) && Plaque.valid_date?(full_date)
      can_circulate = Plaque.can_circulate?(full_number, full_date)
      msj = can_circulate ? "Can circulate on the road" : "Can not circulate on the road"
    else
      msj = "Invalid data"
    end
    respond_to do |format|
      format.html { redirect_to plaques_url(:number => params[:number], :date => params[:date]), notice: msj }
    end
  end
end

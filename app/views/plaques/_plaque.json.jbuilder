json.extract! plaque, :id, :number, :created_at, :updated_at
json.url plaque_url(plaque, format: :json)

require 'test_helper'

class PlaqueTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should return true for valid plaque" do
    assert Plaque.valid_plaque?('pof0991')
  end

  test "should return false for invalid plaque" do
    assert_not Plaque.valid_plaque?('pof0pof')
  end

  test "should return true for valid date" do
    assert Plaque.valid_date?('2017-06-05 06:00:00')
  end

  test "should return false for invalid date" do
    assert_not Plaque.valid_date?('2017-20-20')
  end

  test "should return true if can circulatefor cars" do
    assert Plaque.can_circulate?('pof0991', '2017-06-05 06:00:00')
  end

  test "should return false if can not circulatefor cars" do
    assert_not Plaque.can_circulate?('pof0991', '2017-06-05 07:00:00')
  end

  test "should return true if can circulatefor motorcycles" do
    assert Plaque.can_circulate?('pof991a', '2017-06-05 13:00:00')
  end

  test "should return false if can not circulatefor motorcycles" do
    assert_not Plaque.can_circulate?('pof991a', '2017-06-05 16:30:00')
  end

end
